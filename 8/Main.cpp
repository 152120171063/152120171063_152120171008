/**
* @author:	P�nar K�z�larslan --> e-mail: pinarkzlarslan@gmail.com
* @date  :	11 Kas�m 2019 �ar�amba
* @brief :	Bu kod par�ac��� ak�ll� ev sistemi cihazlar�n�n ayar sim�lasyonudur.
*/
#include "FileReader.h"
#include "MenuManager.h"
using namespace std;

int main(int argc, char** argv) {
	   	 		
	FileReader fr;
	fr.readLine(argc, argv);

	MenuManager m;
	m.createMenu();

	system("pause");
}