#pragma once
#include<string>
#include<iostream>
using namespace std;
class UserInterface {
private:
	static UserInterface* Instance;

public:
	UserInterface();
	~UserInterface();
	UserInterface* getGUI();
	virtual void display();
};