#include "Device.h"
using namespace std;
/**
* @brief :	Bu fonksiyon, ilk de�er atamas� yapar.
*/
Device::Device() {
	id = 0;
	deviceType[30] = {};
	deviceName[30] = {};
	port = 0;
	//state = 0;
}
/**
* @brief :	Bu fonksiyon y�k�c� fonksiyondur.
*/
Device::~Device() {

}
/**
* @brief :	Bu fonksiyon cihaz� a�ar.
*/
void Device::on() {
	state = 1;
}
/**
* @brief :	Bu fonksiyon cihaz� kapat�r.
*/
void Device::off() {
	state = 0;
}
/**
* @brief :	Bu fonksiyon cihaz� a��k-kapal� durumunu d�nd�r�r.
*/
bool Device::checkState() {
	if (state == 1)	return true;
	else return false;
}
/**
* @brief :	Bu fonksiyon men�y� �a��r�r.
*/
void Device::displayMenu() {

}