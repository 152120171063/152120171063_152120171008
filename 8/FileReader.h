#pragma once
#include<iostream>
#include<fstream>
#include<string>
#include "Device.h"
#include "DeviceFactory.h"
using namespace std;

class FileReader {

public:
	FileReader();
	~FileReader();
	void readLine(int argc, char** argv);
};