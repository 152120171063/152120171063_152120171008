#include "MenuManager.h"
Device d;
TV tv;
Air_Conditioner air;
Alarm alarm;
Door door;
Home_Theatre home;
Speakers speak;
Window window;
Camera camera;
FileReader file;
Gas_Sensor gas;
Motion_Sensor motion;
Light light;
/**
* @brief :	Bu fonksiyon, ilk de�er atamas� yapar.
*/
MenuManager::MenuManager() {

}
/**
* @brief :	Bu fonksiyon y�k�c� fonksiyondur.
*/
MenuManager::~MenuManager() {

}
/**
* @brief :	Bu fonksiyon, men� olu�turur.
*/
void MenuManager::createMenu() {

	deviceList.push_back(tv);
	deviceList.push_back(air);
	deviceList.push_back(alarm);
	deviceList.push_back(door);
	deviceList.push_back(speak);
	deviceList.push_back(window);
	deviceList.push_back(camera);
	deviceList.push_back(gas);
	deviceList.push_back(motion);
	deviceList.push_back(light);
	deviceList.push_back(home);

	int tv_secim, air_secim, home_secim, spek_secim, door_secim, window_secim, alarm_secim, camera_secim;
	int fire_secim, gas_secim, motion_secim, light_secim, other_secim, secim;
	
	do
	{
		cout << "  [1] - TV" << endl;
		cout << "  [2] - Air Conditioner " << endl;
		cout << "  [3] - Home Theatre  " << endl;
		cout << "  [4] - Speakers   " << endl;
		cout << "  [5] - Door " << endl;
		cout << "  [6] - Window  " << endl;
		cout << "  [7] - Alarm  " << endl;
		cout << "  [8] - Camera " << endl;
		cout << "  [9] - Fire Sensor  " << endl;
		cout << " [10] - Gas Sensor " << endl;
		cout << " [11] - Motion Sensor  " << endl;
		cout << " [12] - Light  " << endl;
		cout << " [13] - Other " << endl;

		cout << " Enter your choice: ";
		cin >> secim;
		cout << endl << endl;
		system("cls");
		switch (secim)
		{
		case 1:
			cout << "Connection Menu" << endl;
			cout << "[1]-Connect TV" << endl;
			cout << "[2]-Disconnect TV" << endl;
			cout << "[3]-Check State" << endl;
			cout << "[4]-Cancel" << endl;
			cout << "Enter your TV choice : ";
			cin >> tv_secim;
			if (tv_secim == 1){
				d.on();
				cout << "TV is on." << endl;
			}
			else if (tv_secim == 2){
				d.off();
				cout << "TV is off" << endl;
			}
			else if (tv_secim == 3)			
				d.checkState();
			
			else if (tv_secim == 4)	{
				system("cls");
				createMenu();
			}
			cout << endl << endl;
			break;
		case 2:
			cout << "Connection Menu" << endl;
			cout << "[1]-Connect Air Conditioner" << endl;
			cout << "[2]-Disconnect Air Conditioner" << endl;
			cout << "[3]-Check State" << endl;
			cout << "[4]-Cancel" << endl;
			cout << "Enter your Air Conditioner choice : ";
			cin >> air_secim;
			if (air_secim == 1){
				d.on();
				cout << "Air conditioner is on ." << endl;
			}
			else if (air_secim == 2){
				d.off();
				cout << "Air conditioner is off ." << endl;
			}
			else if (air_secim == 3)
				d.checkState();
			
			else if (air_secim == 4){
				system("cls");
				createMenu();
			}
			cout << endl << endl;
			break;
		case 3:
			cout << "Connection Menu" << endl;
			cout << "[1]-Connect Home Theatre" << endl;
			cout << "[2]-Disconnect Home Theatre" << endl;
			cout << "[3]-Check State" << endl;
			cout << "[4]-Cancel" << endl;
			cout << "Enter your Home Theatre choice : ";
			cin >> home_secim;
			if (home_secim == 1){
				d.on();
				cout << "Home Theatre is on ." << endl;
			}
			else if (home_secim == 2){
				d.off();
				cout << "Home Theatre is off ." << endl;
			}
			else if (home_secim == 3)			
				d.checkState();
			
			else if (home_secim == 4){
				system("cls");
				createMenu();
			}
			cout << endl << endl;
			break;
		case 4:
			cout << "Connection Menu" << endl;
			cout << "[1]-Connect Speakers" << endl;
			cout << "[2]-Disconnect Speakers" << endl;
			cout << "[3]-Check State" << endl;
			cout << "[4]-Cancel" << endl;
			cout << "Enter your Speakers choice : ";
			cin >> spek_secim;
			if (spek_secim == 1){
				d.on();
				cout << "Speakers is on ." << endl;
			}
			else if (spek_secim == 2){
				d.off();
				cout << "Speakers is off ." << endl;
			}
			else if (spek_secim == 3)			
				d.checkState();
			
			else if (spek_secim == 4){
				system("cls");
				createMenu();
			}
			cout << endl << endl;
			break;
		case 5:
			cout << "Connection Menu" << endl;
			cout << "[1]-Connect Door" << endl;
			cout << "[2]-Disconnect Door" << endl;
			cout << "[3]-Check State" << endl;
			cout << "[4]-Cancel" << endl;
			cout << "Enter your Door choice : ";
			cin >> door_secim;
			if (door_secim == 1){
				d.on();
				cout << "Door is on ." << endl;
			}
			else if (door_secim == 2){
				d.off();
				cout << "Door is off ." << endl;
			}
			else if (door_secim == 3)			
				d.checkState();
			
			else if (door_secim == 4)			
				system("cls");
				createMenu();
			
			cout << endl << endl;
			break;
		case 6:
			cout << "Connection Menu" << endl;
			cout << "[1]-Connect Window" << endl;
			cout << "[2]-Disconnect Window" << endl;
			cout << "[3]-Check State" << endl;
			cout << "[4]-Cancel" << endl;
			cout << "Enter your Window choice : ";
			cin >> window_secim;
			if (window_secim == 1){
				d.on();
				cout << "Window is on ." << endl;
			}
			else if (window_secim == 2)	{
				d.off();
				cout << "Window is off ." << endl;
			}
			else if (window_secim == 3)			
				d.checkState();
			
			else if (window_secim == 4)	{
				system("cls");
				createMenu();
			}
			cout << endl << endl;
			break;
		case 7:
			cout << "Connection Menu" << endl;
			cout << "[1]-Connect Alarm" << endl;
			cout << "[2]-Disconnect Alarm" << endl;
			cout << "[3]-Check State" << endl;
			cout << "[4]-Cancel" << endl;
			cout << "Enter your Alarm choice : ";
			cin >> alarm_secim;
			if (alarm_secim == 1){
				d.on();
				cout << "Alarm is on ." << endl;
			}
			else if (alarm_secim == 2){
				d.off();
				cout << "Alarm is off ." << endl;
			}
			else if (alarm_secim == 3)			
				d.checkState();
			
			else if (alarm_secim == 4){
				system("cls");
				createMenu();
			}
			cout << endl << endl;
			break;
		case 8:
			cout << "Connection Menu" << endl;
			cout << "[1]-Connect Camera" << endl;
			cout << "[2]-Disconnect Camera" << endl;
			cout << "[3]-Check State" << endl;
			cout << "[4]-Cancel" << endl;
			cout << "Enter your Camera choice : ";
			cin >> camera_secim;
			if (camera_secim == 1){
				d.on();
				cout << "Camera is on ." << endl;
			}
			else if (camera_secim == 2){
				d.off();
				cout << "Camera is off ." << endl;
			}
			else if (camera_secim == 3)			
				d.checkState();
			
			else if (camera_secim == 4){
				system("cls");
				createMenu();
			}
			cout << endl << endl;
			break;
		case 9:
			cout << "Connection Menu" << endl;
			cout << "[1]-Connect Fire Sensor" << endl;
			cout << "[2]-Disconnect Fire Sensor" << endl;
			cout << "[3]-Check State" << endl;
			cout << "[4]-Cancel" << endl;
			cout << "Enter your Fire Sensor choice : ";
			cin >> fire_secim;
			if (fire_secim == 1){
				d.on();
				cout << "Fire Sensor is on ." << endl;
			}
			else if (fire_secim == 2){
				d.off();
				cout << "Fire Sensor is off ." << endl;
			}
			else if (fire_secim == 3)
				d.checkState();
			
			else if (fire_secim == 4){
				system("cls");
				createMenu();
			}
			cout << endl << endl;
			break;
		case 10:
			cout << "Connection Menu" << endl;
			cout << "[1]-Connect Gas Sensor" << endl;
			cout << "[2]-Disconnect Gas Sensor" << endl;
			cout << "[3]-Check State" << endl;
			cout << "[4]-Cancel" << endl;
			cout << "Enter your Gas Sensor choice : ";
			cin >> gas_secim;
			if (gas_secim == 1){
				d.on();
				cout << "Gas Sensor is on ." << endl;
			}
			else if (gas_secim == 2){
				d.off();
				cout << "Gas Sensor is off ." << endl;
			}
			else if (gas_secim == 3)			
				d.checkState();
			
			else if (gas_secim == 4){
				system("cls");
				createMenu();
			}
			cout << endl << endl;
			break;
		case 11:
			cout << "Connection Menu" << endl;
			cout << "[1]-Connect Motion Sensor" << endl;
			cout << "[2]-Disconnect Motion Sensor" << endl;
			cout << "[3]-Check State" << endl;
			cout << "[4]-Cancel" << endl;
			cout << "Enter your Motion Sensor choice : ";
			cin >> motion_secim;
			if (motion_secim == 1){
				d.on();
				cout << "Motion Sensor is on ." << endl;
			}
			else if (motion_secim == 2){
				d.off();
				cout << "Motion Sensor is off ." << endl;
			}
			else if (motion_secim == 3)
				d.checkState();
			
			else if (motion_secim == 4){
				system("cls");
				createMenu();
			}
			cout << endl << endl;
			break;
		case 12:
			cout << "Connection Menu" << endl;
			cout << "[1]-Connect Light" << endl;
			cout << "[2]-Disconnect Light" << endl;
			cout << "[3]-Check State" << endl;
			cout << "[4]-Cancel" << endl;
			cout << "Enter your Light choice : ";
			cin >> light_secim;
			if (light_secim == 1){
				d.on();
				cout << "Light is on ." << endl;
			}
			else if (light_secim == 2){
				d.off();
				cout << "Light is off ." << endl;
			}
			else if (light_secim == 3)
				d.checkState();
			
			else if (light_secim == 4){
				system("cls");
				createMenu();
			}
			cout << endl << endl;
			break;
		case 13:
			cout << "Connection Menu" << endl;
			cout << "[1]-Connect Other" << endl;
			cout << "[2]-Disconnect Other" << endl;
			cout << "[3]-Check State" << endl;
			cout << "[4]-Cancel" << endl;
			cout << "Enter your Other choice : ";
			cin >> other_secim;
			if (other_secim == 1){
				d.on();
				cout << "Other is on ." << endl;
			}
			else if (other_secim == 2){
				d.off();
				cout << "Other is off ." << endl;
			}
			else if (other_secim == 3)			
				d.checkState();
			
			else if (other_secim == 4){
				system("cls");
				createMenu();
			}
			cout << endl << endl;
			break;
		default:
			break;
		}
	} while (true);

}