#pragma once
#include<iostream>
#include<fstream>
#include<string>
#include "Device.h"
#include "MenuManager.h"
using namespace std;
class DeviceFactory {

public:
	DeviceFactory();
	~DeviceFactory();
	void createDevice(Device device);
};