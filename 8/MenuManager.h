#pragma once
#include <list>
#include "Device.h"
#include "TV.h"
#include "Air_Conditioner.h"
#include "Door.h"
#include "Alarm.h"
#include "Home_Theatre.h"
#include"Speakers.h"
#include"Window.h"
#include"Camera.h"
#include"FileReader.h"
#include"Gas_Sensor.h"
#include"Motion_Sensor.h"
#include"Light.h"

class MenuManager {

public:
	MenuManager();
	~MenuManager();
	list<Device> deviceList;
	void createMenu();
};