#ifndef Device_H
#define Device_H

#pragma once
#include<iostream>
#include<fstream>
#include<string>
using namespace std;
class Device{
public:
	int id;
	char deviceType[30];
	char deviceName[30];
	int port;
	bool state;

	Device();
	~Device();
	void on();
	void off();
	bool checkState();
	void displayMenu();
};

#endif // Device_H