#include "TV.h"
/**
* @brief :	Bu fonksiyon, ilk de�er atamas� yapar.
*/
TV::TV():Device(){
	activeChannel = 0;
}
/**
* @brief :	Bu fonksiyon y�k�c� fonksiyondur.
*/
TV::~TV(){

}
/**
* @brief :	Bu fonksiyon input olarak al�nan key'e g�re aktif kanal� g�nceller.
*/
void TV::selecetChannel(int key) {
	activeChannel = key;
}
/**
* @brief :	Bu fonksiyon cihaz� kapat�r.
*/
void TV::off() {
	state = 0;
}
void TV::standBy() {

}