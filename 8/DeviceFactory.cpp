#include "DeviceFactory.h"
#include "TV.h"
#include "Air_Conditioner.h"
#include "Alarm.h"
#include "Door.h"
#include "Camera.h"
#include "Fire_Sensor.h"
#include "Gas_Sensor.h"

/**
* @brief :	Bu fonksiyon, ilk de�er atamas� yapar.
*/
DeviceFactory::DeviceFactory() {

}
/**
* @brief :	Bu fonksiyon y�k�c� fonksiyondur.
*/
DeviceFactory::~DeviceFactory() {

}
/**
* @brief :	Bu fonksiyon, deviceType a g�re cihaz olu�turur.
* @param str :	Input olarak al�nan, kulland���m�z de�i�kendir.
*/
void DeviceFactory::createDevice(Device device) {

	if (strcmp(device.deviceType, "TV") == 0) { // string kar��la�t�rmas� i�in

		TV *tv = new TV;
		tv->id = device.id;
		for (int i = 0; i < sizeof(device.deviceType); i++)
			tv->deviceType[i] = device.deviceType[i];
		for (int i = 0; i < sizeof(device.deviceName); i++)
			tv->deviceName[i] = device.deviceName[i];
		tv->port = device.port;

		cout << "ID: " << tv->id << endl;
		cout << "Type: " << tv->deviceType << endl;
		cout << "Name: " << tv->deviceName << endl;
		cout << "Port: " << tv->port << endl;
		cout << endl;
	}

	if (strcmp(device.deviceType, "Air_Conditioner") == 0) {
		Air_Conditioner *air_Conditioner = new Air_Conditioner;
		air_Conditioner->id = device.id;
		for (int i = 0; i < sizeof(device.deviceType); i++)
			air_Conditioner->deviceType[i] = device.deviceType[i];
		for (int i = 0; i < sizeof(device.deviceName); i++)
			air_Conditioner->deviceName[i] = device.deviceName[i];
		air_Conditioner->port = device.port;

		cout << "ID: " << air_Conditioner->id << endl;
		cout << "Type: " << air_Conditioner->deviceType << endl;
		cout << "Name: " << air_Conditioner->deviceName << endl;
		cout << "Port: " << air_Conditioner->port << endl;
		cout << endl;
	}

	if (strcmp(device.deviceType, "Door") == 0) {
		Door *door = new Door;
		door->id = device.id;
		for (int i = 0; i < sizeof(device.deviceType); i++)
			door->deviceType[i] = device.deviceType[i];
		for (int i = 0; i < sizeof(device.deviceName); i++)
			door->deviceName[i] = device.deviceName[i];
		door->port = device.port;

		cout << "ID: " << door->id << endl;
		cout << "Type: " << door->deviceType << endl;
		cout << "Name: " << door->deviceName << endl;
		cout << "Port: " << door->port << endl;
		cout << endl;
	}

	if (strcmp(device.deviceType, "Alarm") == 0) {
		Alarm *alarm = new Alarm;
		alarm->id = device.id;
		for (int i = 0; i < sizeof(device.deviceType); i++)
			alarm->deviceType[i] = device.deviceType[i];
		for (int i = 0; i < sizeof(device.deviceName); i++)
			alarm->deviceName[i] = device.deviceName[i];
		alarm->port = device.port;

		cout << "ID: " << alarm->id << endl;
		cout << "Type: " << alarm->deviceType << endl;
		cout << "Name: " << alarm->deviceName << endl;
		cout << "Port: " << alarm->port << endl;
		cout << endl;
	}

	if (strcmp(device.deviceType, "Camera") == 0) {
		Camera *camera = new Camera;
		camera->id = device.id;
		for (int i = 0; i < sizeof(device.deviceType); i++)
			camera->deviceType[i] = device.deviceType[i];
		for (int i = 0; i < sizeof(device.deviceName); i++)
			camera->deviceName[i] = device.deviceName[i];
		camera->port = device.port;

		cout << "ID: " << camera->id << endl;
		cout << "Type: " << camera->deviceType << endl;
		cout << "Name: " << camera->deviceName << endl;
		cout << "Port: " << camera->port << endl;
		cout << endl;
	}

	if (strcmp(device.deviceType, "Fire_Sensor") == 0) {
		Fire_Sensor *fire_Sensor = new Fire_Sensor;
		fire_Sensor->id = device.id;
		for (int i = 0; i < sizeof(device.deviceType); i++)
			fire_Sensor->deviceType[i] = device.deviceType[i];
		for (int i = 0; i < sizeof(device.deviceName); i++)
			fire_Sensor->deviceName[i] = device.deviceName[i];
		fire_Sensor->port = device.port;

		cout << "ID: " << fire_Sensor->id << endl;
		cout << "Type: " << fire_Sensor->deviceType << endl;
		cout << "Name: " << fire_Sensor->deviceName << endl;
		cout << "Port: " << fire_Sensor->port << endl;
		cout << endl;
	}
	
	if (strcmp(device.deviceType, "Gas_Sensor") == 0) {
		Gas_Sensor *gas_Sensor = new Gas_Sensor;
		gas_Sensor->id = device.id;
		for (int i = 0; i < sizeof(device.deviceType); i++)
			gas_Sensor->deviceType[i] = device.deviceType[i];
		for (int i = 0; i < sizeof(device.deviceName); i++)
			gas_Sensor->deviceName[i] = device.deviceName[i];
		gas_Sensor->port = device.port;

		cout << "ID: " << gas_Sensor->id << endl;
		cout << "Type: " << gas_Sensor->deviceType << endl;
		cout << "Name: " << gas_Sensor->deviceName << endl;
		cout << "Port: " << gas_Sensor->port << endl;
		cout << endl;
	}

}