// Device.h
#include <iostream> 
#include <fstream>
#include <string>
using namespace std;

class Device {
public:	
	Device();
	~Device();
	int id = 0;
	char device_type[30] = "";
	char device_name[30] = "";
	int port = 0;
	int state;
	void Count();
	bool onOff();
};