// ReadFile.h
#include <iostream> 
#include <fstream>
#include <string>
#include "Device.h"
using namespace std;

class ReadFile {
public:	
	int id;
	char deviceName[30];
	char deviceType[30];
	int port;
	ReadFile();
	~ReadFile();
	void Read();
	int Count();
};